using UnityEngine;
using System.Collections;

public class LearningScript : MonoBehaviour {

	public GameObject capsuleGo;
	Spinner cubeComp;

	// Use this for initialization
	void Start () {

		//capsuleGo = GameObject.Find ("Capsule");
		Debug.Log (capsuleGo);
		cubeComp = GameObject.Find ("Cube").GetComponent<Spinner>();
		Debug.Log (cubeComp);

	}

	// Update is called once per frame
	void Update () {

		if(Input.GetKey (KeyCode.LeftArrow))
		{
			capsuleGo.GetComponent<Spinner>().SpinLeft ();
		}
		if(Input.GetKey (KeyCode.RightArrow))
		{
			capsuleGo.GetComponent<Spinner>().SpinRight ();
		}
		if(Input.GetKey (KeyCode.UpArrow))
		{
			cubeComp.SpinLeft ();
		}
		if(Input.GetKey (KeyCode.DownArrow))
		{
			cubeComp.SpinRight ();
		}

	}
}
