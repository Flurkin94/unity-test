using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class LostState : IStateBase
	{
		private StateManager manager;

		public LostState (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing LostState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp (KeyCode.Space))
			{
				Application.LoadLevel("BeginningScene");
				manager.SwitchState(new BeginState(manager));
			}

		}

		public void ShowIt()
		{


		}

	}
}
