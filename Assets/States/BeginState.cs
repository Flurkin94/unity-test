using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class BeginState : IStateBase
	{
		private StateManager manager;

		private float futureTime =0;

		private int countDown = 0;

		private float screenDuration = 8;

		public BeginState (StateManager managerRef)
		{
			manager = managerRef;
			futureTime = screenDuration + Time.realtimeSinceStartup;

			if(Application.loadedLevelName != "scene0")
				Application.LoadLevel ("scene0");
		}

		public void StateUpdate()
		{
			float rightNow = Time.realtimeSinceStartup;
			countDown = (int)futureTime - (int)rightNow;
			if(Input.GetKeyUp(KeyCode.Space) || countDown <= 0)
			{
				Switch();
			}

		}

		public void ShowIt()
		{
			if(GUI.Button(new Rect(10,10,150,100), "Press to Play"))
				{
					Switch ();
				}
			GUI.Box (new Rect (Screen.width - 60,10,50,25), countDown.ToString());
		}

		void Switch()
		{
			Application.LoadLevel("Scene1");
			manager.SwitchState (new PlayStateScene1_1 (manager));
		}
	}
}
