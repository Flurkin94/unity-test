using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class LostStateScene2 : IStateBase
	{
		private StateManager manager;

		public LostStateScene2 (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing LostState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp (KeyCode.Space))
			{
				Application.LoadLevel("scene2");
				manager.SwitchState(new PlayStateScene2(manager));
			}
			if (Input.GetKeyUp (KeyCode.Return))
			{
				Application.LoadLevel ("BeginningScene");
				manager.SwitchState(new BeginState(manager));
			}

		}

		public void ShowIt()
		{


		}

	}
}
