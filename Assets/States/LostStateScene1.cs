using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class LostStateScene1 : IStateBase
	{
		private StateManager manager;

		public LostStateScene1 (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing LostState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp (KeyCode.Space))
			{
				Application.LoadLevel("scene1");
				manager.SwitchState(new PlayStateScene1_1(manager));
			}
			if (Input.GetKeyUp (KeyCode.Return))
			{
				Application.LoadLevel ("BeginningScene");
				manager.SwitchState(new BeginState(manager));
			}

		}

		public void ShowIt()
		{


		}

	}
}
