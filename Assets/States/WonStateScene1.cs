using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class WonStateScene1 : IStateBase
	{
		private StateManager manager;

		public WonStateScene1 (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing WonState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp (KeyCode.Space))
			{
				manager.SwitchState(new PlayStateScene2 (manager));
			}

		}

		public void ShowIt()
		{

		}

	}
}
