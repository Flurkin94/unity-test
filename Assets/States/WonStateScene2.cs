using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class WonStateScene2 : IStateBase
	{
		private StateManager manager;

		public WonStateScene2 (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing WonState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp (KeyCode.Space))
			{
				manager.SwitchState(new BeginState (manager));
			}

		}

		public void ShowIt()
		{

		}

	}
}
