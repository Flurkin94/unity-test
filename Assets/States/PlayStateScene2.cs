using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class PlayStateScene2 : IStateBase
	{
		private StateManager manager;

		public PlayStateScene2 (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing PlayState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp(KeyCode.Space))
			{
				manager.SwitchState(new WonStateScene2 (manager));
			}

				if (Input.GetKeyUp (KeyCode.Return))
			{
				manager.SwitchState (new LostStateScene2 (manager));
			}
		}

		public void ShowIt()
		{

		}

	}
}
