using UnityEngine;
using Assets.Code.Interfaces;

namespace Assests.Code.States
{
	public class PlayStateScene1_1 : IStateBase
	{
		private StateManager manager;

		public PlayStateScene1_1 (StateManager managerRef)
		{
			manager = managerRef;
			Debug.Log("Constructing PlayState");
		}

		public void StateUpdate()
		{

			if (Input.GetKeyUp(KeyCode.Space))
			{
				manager.SwitchState(new PlayStateScene1_2 (manager));
			}

				if (Input.GetKeyUp (KeyCode.Return))
			{
				manager.SwitchState (new LostStateScene1 (manager));
			}
		}

		public void ShowIt()
		{

		}

	}
}
